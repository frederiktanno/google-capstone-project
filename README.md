# How Does a Bike-Share Navigate Speedy Success? 
# A Case of Cyclistic Bike-Sharing Company

**Prepared By: Laurensius Frederik Tanno**

## 1. Overview

In 2016, Cyclistic launched a successful bike-share offering. Since then, the program has grown to a fleet of 5,824 bicycles that are geotracked and locked into a network of 692 stations across Chicago. The bikes can be unlocked from one station and returned to any other station in the system anytime. Until now, Cyclistic’s marketing strategy relied on building general awareness and appealing to broad consumer segments. One approach that helped make these things possible was the flexibility of its pricing plans: single-ride passes, full-day passes, and annual memberships. Customers who purchase single-ride or full-day passes are referred to as casual riders. Customers who purchase annual memberships are Cyclistic members. Cyclistic’s finance analysts have concluded that annual members are much more profitable than casual riders.

Although the pricing flexibility helps Cyclistic attract more customers, Moreno believes that maximizing the number of annual members will be key to future growth. Rather than creating a marketing campaign that targets all-new customers, Moreno believes there is a very good chance to convert casual riders into members. She notes that casual riders are already aware of the Cyclistic program and have chosen Cyclistic for their mobility needs. Moreno has set a clear goal: Design marketing strategies aimed at converting casual riders into annual members. In order to do that, however, the marketing analyst team needs to better understand how annual members and casual riders differ, why casual riders would buy a membership, and how digital media could affect their marketing tactics. Moreno and her team are interested in analyzing the Cyclistic historical bike trip data to identify trends.

For a more detailed background of the case study, you can find the summary [here](Case_Study_Summary.pdf)

## 2. Business Task

The main problem we are trying to solve is how to entice casual riders to purchase Cyclistic’s annual membership programme. We then would compare the differences in behavior between membership riders and casual riders. The analysis between these two customer segments would be useful in creating insights on what benefits the memberships would give to casual riders, and how the marketing team should focus their marketing strategy on. The main business question which needs to be answered would be **How do casual customers and member customers use Cyclistic rides differently?**

## 3. Data Preparation and Definition

The data used for the analysis is the company’s historical trip data between May 2022 and May 2023 period. The data was retrieved from an [open source S3 bucket](https://divvy-tripdata.s3.amazonaws.com/index.html), and were stored in a CSV format. The trip data are divided into 12 different CSV files, with each file containing a month’s worth of trip data. It is important to note that the data provided is not Cyclistic’s own data, since it is a fictional company, however the dataset represents the required information to answer the business questions outlined above. The data was provided by Motivate International Inc, and has been licensed as public data. Accordingly, the datasets do not contain the riders’ personal information, and simply stores the customers’ ride log. This means that pass purchases would not be able to be connected to credit card numbers to determine if casual riders live in the Cyclistic service area or if they have purchased multiple single passes.

The following table lists the name of columns and data types retrieved from the public dataset



|No|Column name|Type|Description|
| - | - | - | - |
|1|Ride\_id|String|Ride activity’s identification key|
|2|Rideable\_type|String|Type of bike being used|
|3|started\_at|timestamp|The start time of the ride|
|4|ended\_at|timestamp|The end time of the ride|
|5|start\_station\_nam e|string|The name of the starting station of the ride|
|6|start\_station\_id|string|The id string of the start station|
|7|end\_station\_nam e|string|The name of the ending station of the ride|
|8|end\_station\_id|string|The id string of the end station|
|9|start\_lat|decimal|The starting latitude of the ride|
|10|start\_lng|decimal|The starting longitude of the ride|
|11|end\_lat|decimal|The ending latitude of the ride|
|12|end\_lng|decimal|The ending longitude of the ride|
|13|member\_casual|string|Membership status of the customer|

## 4. Data Processing

For purposes of faster and easier analysis, the CSV data were imported into a PostgreSQL database stored in the analyst’s local device. The data is stored in the *postgres* database, and is mapped in the *cyclistic\_ride\_data* table. PostgreSQL was chosen as the database storage engine and processing medium because of its open source availability, superior performance for processing large amounts of data, and the analyst’s knowledge and positive previous experiences with the query language. The data definition and indexing queries can be seen in [this file](cyclistic_ddl.sql). The transformed dataset can be found in this [Kaggle Dataset](https://www.kaggle.com/datasets/frederiktanno/google-capstone-project-cylcistic-rides)

In addition to the main datasets taken from the CSV files, the *cyclistic\_ride\_data* table also stores several additional columns as metadata. The metadata columns are as follows:



|No|Column Name|Type|Description|
| - | - | - | - |
|1|id|bigserial|The primary key of the table. Used as the unique identifier for each individual data row|
|2|created\_at|timestamp|Records the time when the data was inserted into the database|
|3|updated\_at|timestamp|Records the time whenever any column in each row was last modified|

The dataset integrity was defined mainly by the use of standardized data type for data point storage. For example, time-related data such as starting time and end time of the rides are stored in a unified timestamp format (YYYY-MM-DD HH:mm:ss). However, there are also inconsistencies of data format in other columns. These inconsistencies result in dirty data for some of the data rows. Some of the identified dirty data are the following:

- The start\_station\_id column does not have a unified format for the id value, with some rows having a 5-digits length of id number, while others having 12 characters length of combined digits and letters. However, the start and end station data values are not relevant to our analysis.
- The value of ending ride time being smaller than the starting ride time. This would negatively affect the result of the ride time analysis.
- The value of ending ride time being the same as the starting ride time.
- Absence of several values for starting and ending station data. There are a total of 1,466,622 rows of data with missing start and end station ID, with 1,459,734 rows belonging to electric bike rides. It can be assumed that electronic bikes do not collect starting and ending station information in the first place, therefore it was decided to include those incomplete data in the analysis. On top of that, starting and ending station data are irrelevant to our scope of analysis.
- Absence of several values for ending latitudes and longitudes.
- Ride times exceeding 1 day, or in this analysis’ case, exceeding 1440 minutes

To ensure that the data we analyze is clean, the dirty data would need to be fixed or filtered out. The calculation of riding time was done using an SQL query by subtracting the difference between *ended\_at* and *started\_at* values, and then store the calculation value in a new column called *ride\_time*. The calculation, which is measured in minutes, uses the following query:

```update cyclistic_ride_data set ride_time = round((extract(epoch from ended_at - started_at)/60),2) where ride_time is null;```

Additionally, an additional column called *day\_of\_week* was created in order to identify the name of the day when the customers started their rides. The day calculation was achieved through the use of the following query:

```update cyclistic_ride_data set day_of_week = to_char(started_at,'Day') where day_of_week is null;```

In total, the Cyclistic dataset, which records riding data between May 2022 and May 2023 contains 6,463,888 rows of data. The analysis of the data is conducted on the cleaned up dataset. Some of the cleaned up data include those with a ride time smaller than or equals to 0 minutes, and a ride time of more than 1440 minutes (or 24 hours). The second filter omits data which have no *end\_lat* and *end\_lng* data. In total, there are 7,743 rows of data which were filtered out from this category.

## 5. Analysis

The analysis was conducted in order to discover possible distinctions in ride behaviour between casual customers and member customers. A full list of queries used for the analysis can be found in [this file](analytics.sql). The data visualization was done using Tableau Public, and you could view the Tableau Viz [here](https://public.tableau.com/views/Cyclistic_Analysis_16879409609340/CasualvsMembershipNumberofRidesPerHour?:language=en-US&publish=yes&:display_count=n&:origin=viz_share_link)

The total number of analyzed rides between May 2022 and May 2023 was 6,456,445 distinct rides. These rides are considered distinct because each of them has unique ride ID values. Of these rides, 2,586,239 were from casual riders, while 3,870,206 rides were from membership riders. The pie chart below illustrates the division of rides based on the membership types.

![](Number_of_Rides_Casual_vs_Member.png)

Figure 5.1 Number of Rides based on membership type

From here, we can see that nearly 60% of Cyclistic customers have already signed for the annual membership programme. Therefore, the initial suggestion of shifting our marketing strategy towards membership customers and converting new customers into members is a sound strategy. Furthermore, we can also analyze the bike preferences of both customer types. As shown in the figure below, casual members have a stronger preference towards electric bikes compared to both docked and classic bikes, while annual members have a more balanced preference between classic and electric bikes. In total, there seem to be a prevalent preference on electric bikes for both customers.

![](Number_of_Rides_by_Membership_and_Ride_Type.png)

Figure 5.2 Bike type preferences based on membership type

The following analysis focuses on the average length of rides for both customer types. As we can see in the figure below, casual riders tend to rent our rides for a longer period of time compared to annual members. Casual riders have an average riding time of around 21 minutes, while annual members only use our rides for 12 minutes on average.

![](Average_Ride_Time_by_Member_Type.png)

Figure 5.3 Average Ride Time by Member Type

The previous insight raises new questions on why casual riders tend to rent the rides for a longer period of time. Therefore, a behavioral analysis on these two customers is needed. The figure below shows the difference in behaviour the length of rides between the customers, based on the week days.

![](Casual_vs_Membership_Average_Ride_Time_by_Day.png)

Figure 5.4 Daily Average Ride Time

As we can see, membership riders have a more stable ride time during the weekdays as compared to casual riders. This implies that membership riders tend to use their bikes as a way for commuting to and from their workplace. On the other hand, casual riders tend to have significantly longer rides on the weekends, implying that they may be using the rides more for leisurely activities. We can further test these hypotheses by analyzing the total number of rides the customers take during these weekdays, as well as the total number of rides per hour.

![](Casual_vs_Membership_Number_of_Rides_by_Day.png)

Figure 5.5 Number of Rides by Day

![](Casual_vs_Membership_Number_of_Rides_Per_Hour.png)

Figure 5.6 Number of Rides by hour

The two figures above show that there were an upwards trend of usage for membership riders during the weekdays and a slight reduction during the weekends, while the inverse was true for the casual riders. Furthermore, there were trends of increasing rides for membership riders during the common commuting hours (6am-8am and 3pm-6pm), with peak rides occurring at 5pm. On the other hand, ridership of casual members have a more stable increase by the hour throughout the day, with rides becoming less frequent starting from 6pm for both types of customers. These two analyses further supports the earlier hypothesis, in which **casual riders tend to ride for more leisurely activities, while membership riders use our rides for commuting**.

## 6. Conclusion and Recommendations

From the analysis conducted on historical ride data between May 2022 and May 2023, we can see the difference in ride behaviours between casual customers and members. Some of the key insights are as follows:

- On average, casual members tend to use our bikes for a longer period of time (21.2 minutes), compared to annual members (12.2 minutes)
- There is a trend among our annual members on when they use our bikes. The members tend to use our bikes for commuting during weekdays and during peak commuting times (6am-8am and 3pm-6pm).
- Casual members tend to use our bikes for leisurely activities, with large spikes in usage during the weekends, and steadily rising usage throughout the day between 5 am to 6 pm.
- Low activities for both customers on hours above 7pm.
- Higher preference on electric bikes compared to classic bikes. This preference is especially prevalent on casual members

Based on these discoveries, we can give several recommendations on how to entice casual customers into potential annual members. The first recommendation is to **give membership bonuses for rides during the weekends, in order to attract customers who use bicycles for leisurely activities**. On top of that, we can offer **bonuses on electric bikes for recently joined members, such as reduced fees for the first week of rides after starting their membership**. FInally, An insight on the product popularity may be used as a consideration on product offering. The management may need to consider the continuous cost and benefit of offerign the docked bike type, as it is considerably less popular compared with the other two bike types.
