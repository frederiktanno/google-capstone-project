

-- public.cyclistic_ride_data definition

-- Drop table

-- DROP TABLE public.cyclistic_ride_data;

CREATE TABLE public.cyclistic_ride_data (
	id int8 NOT NULL DEFAULT nextval('cyclistic_ride_data_seq'::regclass),
	ride_id varchar NULL,
	rideable_type varchar NULL,
	started_at timestamp NULL,
	ended_at timestamp NULL,
	start_station_name varchar NULL,
	start_station_id varchar NULL,
	end_station_name varchar NULL,
	end_station_id varchar NULL,
	start_lat numeric(15, 8) NULL,
	end_lat numeric(15, 8) NULL,
	start_lng numeric(15, 8) NULL,
	end_lng numeric(15, 8) NULL,
	member_casual varchar NULL,
	created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at timestamp NULL,
	ride_time numeric(17, 4) NULL,
	day_of_week varchar NULL,
	CONSTRAINT cyclistic_ride_data_pk PRIMARY KEY (id)
);
CREATE INDEX cyclistic_ride_data_member_casual_idx ON public.cyclistic_ride_data USING btree (member_casual, start_lat, end_lat, ride_time, day_of_week);
CREATE INDEX cyclistic_ride_data_started_at_idx ON public.cyclistic_ride_data USING btree (started_at, ended_at);

-- Table Triggers
-- create a trigger to automatically update timestamp every time there is an update operation on the table
create trigger update_cyclistic_time before
update
    on
    public.cyclistic_ride_data for each row execute function update_changetimestamp_column();