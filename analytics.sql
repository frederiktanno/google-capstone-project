
-- populate the ride_time column with the time difference between started_at and ended_at
-- the ride time is measured in minutes
update cyclistic_ride_data set ride_time = round((extract(epoch from ended_at - started_at)/60),2) 
where ride_time is null ;

-- populate the day of week column with the day of week name of the ride start date
update cyclistic_ride_data set day_of_week = to_char(started_at,'Day') where day_of_week is null;

-- day_of_week column was discovered to have trailing spaces on much of the rows. This query removes those trailing spaces
update cyclistic_ride_data set day_of_week = trim(trailing ' ' from day_of_week);

/*
	Count the number of dirty data, based on data with no ending lat and lng, as well as ride time
	smaller than or equal than 0, or ride time longer than 1 day
*/
select count(*)
-- ,rideable_type 
from cyclistic_ride_data 
where
-- (ride_time <= 0 or ride_time > 1440)
-- and start_station_id is null 
-- ride_time > 0 and ride_time <= 1440
(end_lat is null
and end_lng is null)
or ride_time <= 0 or ride_time > 1440
-- and end_station_id is null
-- group by rideable_type 
;

-- calculate the average ride time by member type
select avg(ride_time) as average,
member_casual 
from cyclistic_ride_data 
where ride_time > 0 and ride_time <= 1440
and end_lat is not null and end_lng is not null
group by member_casual;

-- calculate the amount of customers by member type
select 
count(*) as customer_count,
member_casual
from cyclistic_ride_data
where ride_time > 0 and ride_time <= 1440
and end_lat is not null and end_lng is not null
group by member_casual,rideable_type ;

-- calculate the average ride time by day and member types
select * from(
	select 
	avg(ride_time) as average,
	day_of_week,
	member_casual
	from cyclistic_ride_data
	where ride_time > 0 and ride_time <= 1440
	and end_lat is not null
	and end_lng is not null
	group by day_of_week,member_casual
) as t1
order by 
CASE
          WHEN day_of_week = 'Monday' THEN 1
          WHEN day_of_week = 'Tuesday' THEN 2
          WHEN day_of_week = 'Wednesday' THEN 3
          WHEN day_of_week = 'Thursday' THEN 4
          WHEN day_of_week = 'Friday' THEN 5
          WHEN day_of_week = 'Saturday' THEN 6
          WHEN day_of_week = 'Sunday' THEN 7
    end
 ASC
;

-- count the amount of rides during each week day, grouped by member types
select * from(
	select 
	count(ride_time) as total_rides,
	day_of_week,
	member_casual
	from cyclistic_ride_data
	where ride_time > 0 and ride_time <= 1440
	and end_lat is not null
	and end_lng is not null
	group by day_of_week,member_casual
) as t1
order by 
CASE
          WHEN day_of_week = 'Monday' THEN 1
          WHEN day_of_week = 'Tuesday' THEN 2
          WHEN day_of_week = 'Wednesday' THEN 3
          WHEN day_of_week = 'Thursday' THEN 4
          WHEN day_of_week = 'Friday' THEN 5
          WHEN day_of_week = 'Saturday' THEN 6
          WHEN day_of_week = 'Sunday' THEN 7
    end
 ASC
;

-- count the amount of rides during each hour of the day, grouped on member type
select 
	count(*) as total_ride,
	extract(hour from started_at) as hour,
	member_casual
	from cyclistic_ride_data
	where ride_time > 0 and ride_time <= 1440
	and end_lat is not null
	and end_lng is not null
	group by hour,member_casual
	;